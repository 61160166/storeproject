/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeprogect.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author Nopparuth
 */
public class ProductDao implements DaoInterface<Product > {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Product (Name,Type, Price, Amount) VALUES (?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setInt(2,object.getAmount());
            stmt.setDouble(3, object.getPrice());
            stmt.setString(4, object.getType());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();
            
            while(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Product_ID, Name, Amount, Price, Type FROM Product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("Product_ID");
               String Name = result.getString("Name");
               int Amount = result.getInt("Amount");
               double price = result.getDouble("Price");
               String type = result.getString("Type");
               Product product = new Product(id, Name, Amount, price, type);
               list.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Product_ID, Name, Amount, Price, Type FROM Product WHERE Product_ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()){
               int pid = result.getInt("Product_ID");
               String Name = result.getString("Name");
               int Amount = result.getInt("Amount");
               double price = result.getDouble("Price");
               String type = result.getString("Type");
               Product product = new Product(pid, Name, Amount, price, type);
               return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Product WHERE Product_ID = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Product SET Name = ? ,Amount = ?,Price = ? ,Type = ? WHERE Product_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setInt(2,object.getAmount());
            stmt.setDouble(3, object.getPrice());
            stmt.setString(4, object.getType());
            stmt.setInt(5,object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Product(-1, "KafaeYen", 20, 20.0, "coffee"));
        System.out.println("id: "+ id);
        Product lastProduct = dao.get(id);
        System.out.println("last product: "+lastProduct);
        lastProduct.setPrice(100);
        int row = dao.update(lastProduct);
        Product updateProduct = dao.get(id);
        System.out.println("update product: " + updateProduct);
        dao.delete(id);
        Product deleteProduct = dao.get(id);
        System.out.println("delete product: " + deleteProduct);
    } 
    
}
