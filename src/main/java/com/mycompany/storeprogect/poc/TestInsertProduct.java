/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprogect.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author Nopparuth
 */
public class TestInsertProduct {
     public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "INSERT INTO Product (Name,Type, Price, Amount) VALUES (?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Product product = new Product(-1, "Oh leing", 20, 20.0,"coffee");
            stmt.setString(1,product.getName());
            stmt.setInt(2,product.getAmount());
            stmt.setDouble(3, product.getPrice());
            stmt.setString(4, product.getType());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            while(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " id: " + id);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
